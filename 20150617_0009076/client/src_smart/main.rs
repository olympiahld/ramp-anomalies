function main() {
  example.p1.text = "Ramp Version: " ++ rampGetProperty("config._ramp_version");
  example.p2.text = "OS Name: " ++ rampGetProperty("config._os_name");
  example.p3.text = "OS Version: " ++ rampGetProperty("config._os_version");
  example.p4.text = "Device Model: " ++ rampGetProperty("config._device_model");
  example.p5.text = "Device Make: " ++ rampGetProperty("config._device_make");
  goto("example");
}