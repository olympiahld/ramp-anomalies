# The Issue

Ramp UXML performace.

# The App

Demonstrates updated two groups of Labels inside one form, the speed
should be consistent for both groups as they are equal in size, but
the second group takes exponentially longer.

# Reported

By Johan on Wednesday, 8 July, 2015.
