function main() {
  example.p1.text = "Ramp Version: " ++ rampGetProperty("config._ramp_version");
  example.p2.text = "OS Name: " ++ rampGetProperty("config._os_name");
  example.p3.text = "OS Version: " ++ rampGetProperty("config._os_version");
  example.p4.text = "Device Model: " ++ rampGetProperty("config._device_model");
  example.p5.text = "Device Make: " ++ rampGetProperty("config._device_make");
  example.p6.text = "Test Status: Starting";
  // goto("example");
  goto_big();
}

function goto_big() {
  group_size = 1000; // dont make more than 1000
  start_time = timerGetTime();
  for(i = 1; i <= group_size; i = i + 1) {
    id = "big.t" ++ i ++ ".text";
    text = uxmlGet(id);
    uxmlSet(id, ".");
  }
  end_time = timerGetTime();
  big.p1.text = "Total time for first group (ms): " ++ (end_time - start_time);
  start_time = timerGetTime();
  for(i = (group_size+1); i <= (group_size*2); i = i + 1) {
    id = "big.t" ++ i ++ ".text";
    text = uxmlGet(id);
    uxmlSet(id, ".");
  }
  end_time = timerGetTime();
  big.p2.text = "Total time for next group (ms): " ++ (end_time - start_time);
  big.p3.text = "Group Size: " ++ group_size;
  goto("big");
}