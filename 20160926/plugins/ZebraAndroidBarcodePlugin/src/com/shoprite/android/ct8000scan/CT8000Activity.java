package com.shoprite.android.ct8000scan;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.ArrayList;

import com.symbol.emdk.EMDKManager;
import com.symbol.emdk.EMDKManager.EMDKListener;
import com.symbol.emdk.EMDKManager.FEATURE_TYPE;
import com.symbol.emdk.barcode.BarcodeManager.ConnectionState;
import com.symbol.emdk.barcode.BarcodeManager;
import com.symbol.emdk.barcode.ScanDataCollection;
import com.symbol.emdk.barcode.ScannerException;
import com.symbol.emdk.barcode.ScannerResults;
import com.symbol.emdk.barcode.BarcodeManager.ScannerConnectionListener;
import com.symbol.emdk.barcode.ScanDataCollection.ScanData;
import com.symbol.emdk.barcode.Scanner.DataListener;
import com.symbol.emdk.barcode.Scanner.StatusListener;
import com.symbol.emdk.barcode.StatusData.ScannerStates;
import com.symbol.emdk.barcode.ScannerInfo;
import com.symbol.emdk.barcode.StatusData;
import com.virtualmobiletech.plugin.PluginActivity;
import com.virtualmobiletech.plugin.PluginActivityModel;
import com.virtualmobiletech.plugins.PluginUtils;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;

public class CT8000Activity extends PluginActivityModel implements
		ScannerConnectionListener, DataListener, StatusListener, EMDKListener {

	private static String callback;

	@Override
	protected void doCreate(Bundle savedInstanceState) {
		super.doCreate(savedInstanceState);

		// Log.wtf("Hashcode - ",
		// String.valueOf(CT8000Service.currentActivity.hashCode()));
		Log.wtf("Object - ", String.valueOf(CT8000Service.currentActivity));

		try {
			if (CT8000Service.currentActivity == null) {

				Util util = new Util();

				if (!util.isMyServiceRunning(CT8000Service.class)) {
					PluginUtils
							.getInstance()
							.getContext()
							.startService(
									new Intent(PluginUtils.getInstance()
											.getContext(), CT8000Service.class));

					if (CT8000Service.currentActivity == null)
						throw new Exception();
				}
			}
		} catch (Exception e) {
			StringWriter something = new StringWriter();
			PrintWriter pw = new PrintWriter(something);
			e.printStackTrace(pw);
			something.toString();
			Log.wtf("doCreate Exception ",
					"CT8000Service.currentActivity is null");
		}
	}

	@Override
	protected void doResume() {
		super.doResume();

		if (CT8000Service.manager != null) {

			// Initialize barcodeManager
			CT8000Service.barcodeManager = (BarcodeManager) CT8000Service.manager
					.getInstance(FEATURE_TYPE.BARCODE);

			// Add connection listener
			if (CT8000Service.barcodeManager != null) {
				CT8000Service.barcodeManager
						.addConnectionListener(CT8000Service.currentActivity);
			}

			// Initialize scanner
			CT8000Service.initScanner();

			if (CT8000ScanPlugin.scannerState == true) {
				CT8000Service.startScan();
			} else {
				CT8000Service.stopScan();
			}
		}
		PluginActivity.getCurrentRampActivity().finishActivity(
				PluginUtils.IGNORE_REQUEST_CODE);
	}

	public static String getCallback() {
		return callback;
	}

	public static void setCallback(String callback) {
		CT8000Activity.callback = callback;
	}

	@Override
	public void onClosed() {

		if (CT8000Service.manager != null) {

			// Remove connection listener
			if (CT8000Service.barcodeManager != null) {
				CT8000Service.barcodeManager
						.removeConnectionListener(CT8000Service.currentActivity);
				CT8000Service.barcodeManager = null;
			}

			// Release all the resources
			CT8000Service.manager.release();
			CT8000Service.manager = null;
		}

	}

	@Override
	public void onOpened(EMDKManager emdkManager) {

		CT8000Service.manager = emdkManager;

		// Acquire the barcode manager resources
		CT8000Service.barcodeManager = (BarcodeManager) emdkManager
				.getInstance(FEATURE_TYPE.BARCODE);

		// Add connection listener
		if (CT8000Service.barcodeManager != null) {
			CT8000Service.barcodeManager
					.addConnectionListener(CT8000Service.currentActivity);
		}

	}

	@Override
	public void onConnectionChange(ScannerInfo scannerInfo,
			ConnectionState connectionState) {

	}

	@Override
	public void onData(ScanDataCollection scanDataCollection) {

		if ((scanDataCollection != null)
				&& (scanDataCollection.getResult() == ScannerResults.SUCCESS)) {
			ArrayList<ScanData> scanData = scanDataCollection.getScanData();
			for (ScanData data : scanData) {

				String dataString = data.getData();
				Log.wtf("Shoprite", "dataString: " + dataString);
				CT8000ScanPlugin.scannerState = false;
				CT8000Service.stopScan();
				if (callback != null) {
					PluginUtils.getInstance().executeFunctionWithParams(
							callback, new Object[] { dataString });
				}
			}
		}
	}

	@Override
	public void onStatus(StatusData statusData) {

		ScannerStates state = statusData.getState();
		String statusString = "";

		switch (state) {
		case IDLE:
			try {
				// An attempt to use the scanner continuously and rapidly (with
				// a delay < 100 ms between scans)
				// may cause the scanner to pause momentarily before resuming
				// the scanning.
				// Hence add some delay (>= 100ms) before submitting the next
				// read.
				try {
					Thread.sleep(100);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
				if (CT8000ScanPlugin.scannerState == true)
					CT8000Service.scanner.read();
			} catch (ScannerException e) {
				statusString = e.getMessage();
			}

			break;
		case WAITING:
			statusString = "Scanner is waiting for trigger press...";
			break;
		case SCANNING:
			statusString = "Scanning...";
			break;
		case DISABLED:
			statusString = statusData.getFriendlyName() + " is disabled.";
			break;
		case ERROR:
			statusString = "An error has occurred.";
			break;
		default:
			break;
		}
	}
}
