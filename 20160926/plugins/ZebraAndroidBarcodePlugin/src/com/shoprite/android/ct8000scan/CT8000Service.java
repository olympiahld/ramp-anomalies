package com.shoprite.android.ct8000scan;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.List;

import com.symbol.emdk.EMDKManager;
import com.symbol.emdk.EMDKManager.EMDKListener;
import com.symbol.emdk.EMDKManager.FEATURE_TYPE;
import com.symbol.emdk.EMDKResults;
import com.symbol.emdk.barcode.BarcodeManager;
import com.symbol.emdk.barcode.Scanner;
import com.symbol.emdk.barcode.ScannerConfig;
import com.symbol.emdk.barcode.ScannerException;
import com.symbol.emdk.barcode.Scanner.TriggerType;
import com.symbol.emdk.barcode.ScannerInfo;
import com.symbol.emdk.barcode.ScannerInfo.DecoderType;
import com.virtualmobiletech.plugin.PluginActivity;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.util.Log;

public class CT8000Service extends Service implements EMDKListener {

	public static BarcodeManager barcodeManager;
	public static CT8000Activity currentActivity;
	public static EMDKManager manager;
	public static Scanner scanner;

	@Override
	public void onCreate() {
		super.onCreate();

		try {

			EMDKResults results = EMDKManager
					.getEMDKManager(PluginActivity.getCurrentRampActivity(),
							CT8000Service.this);

			Log.d("Shoprite", "results status code: " + results.statusCode);

			if (results.statusCode != EMDKResults.STATUS_CODE.SUCCESS)
				throw new Exception();

		} catch (Exception e) {
			StringWriter something = new StringWriter();
			PrintWriter pw = new PrintWriter(something);
			e.printStackTrace(pw);
			something.toString();
			Log.wtf("Exception - " + "onCreated", "EMDKResults Failed "
					+ something);
		}
	}

	@Override
	public IBinder onBind(Intent intent) {

		return null;
	}

	@Override
	public int onStartCommand(Intent intent, int flags, int startId) {

		Log.wtf("onStartCommand", "START_NOT_STICKY");
		currentActivity = new CT8000Activity();

		return START_NOT_STICKY;
	}

	@Override
	public boolean stopService(Intent name) {

		Log.wtf("stopService", "not null");
		return super.stopService(name);
	}

	@Override
	public void onDestroy() {
		super.onDestroy();

		Log.wtf("Service", "onDestroy");

		deInitScanner();

		// Release all the resources
		if (manager != null) {
			manager.release();
			manager = null;
		}
	}

	public static void initScanner() {

		if (scanner == null) {

			// TC8000 - 0 for scanner
			// TC 75 - 1 for scanner

			int found = -1;

			List<ScannerInfo> list = barcodeManager.getSupportedDevicesInfo();
			for (int i = 0; i < list.size(); i++) {
				ScannerInfo info = list.get(i);
				if (info.getDecoderType() == DecoderType.TWO_DIMENSIONAL) {
					found = i;
				}
			}

			if (found == -1) {
				Log.wtf("Shoprite",
						"No suitable TWO_DIMENSIONAL ScannerInfo found!");
				return;
			}

			scanner = barcodeManager.getDevice(list.get(found));

			if (scanner != null) {

				scanner.addDataListener(currentActivity);
				scanner.addStatusListener(currentActivity);
				setupDevice();
				try {
					scanner.enable();
					scanner.triggerType = TriggerType.HARD;

				} catch (ScannerException e) {

					StringWriter something = new StringWriter();
					PrintWriter pw = new PrintWriter(something);
					e.printStackTrace(pw);
					something.toString();
					Log.wtf("Exception - " + "initScanner",
							something.toString());
				}
			} else {
				Log.wtf("Shoprite", "No Scanner found!");
				return;
			}
		}
	}

	private void deInitScanner() {

		if (scanner != null) {

			try {

				scanner.cancelRead();
				scanner.disable();

			} catch (ScannerException e) {

				// textViewStatus.setText("Status: " + e.getMessage());
			}
			scanner.removeDataListener(currentActivity);
			scanner.removeStatusListener(currentActivity);
			try {
				scanner.release();
			} catch (ScannerException e) {

				// textViewStatus.setText("Status: " + e.getMessage());
			}

			scanner = null;
		}
	}

	public static void startScan() {

		if (scanner == null) {
			initScanner();
		}

		if (scanner != null) {
			try {
				scanner.read();
			} catch (ScannerException e) {

				StringWriter something = new StringWriter();
				PrintWriter pw = new PrintWriter(something);
				e.printStackTrace(pw);
				something.toString();
				Log.wtf("Exception - " + "startScan", something.toString());
			}
		}
	}

	public static void stopScan() {

		if (scanner != null) {

			try {
				scanner.cancelRead();
			} catch (ScannerException e) {
				StringWriter something = new StringWriter();
				PrintWriter pw = new PrintWriter(something);
				e.printStackTrace(pw);
				something.toString();
				Log.wtf("Exception - " + "stopScan", something.toString());
			}
		}
	}

	@Override
	public void onClosed() {

		if (manager != null) {

			// Remove connection listener
			if (barcodeManager != null) {
				barcodeManager.removeConnectionListener(currentActivity);
				barcodeManager = null;
			}

			// Release all the resources
			manager.release();
			manager = null;
		}

	}

	@Override
	public void onOpened(EMDKManager emdkManager) {

		manager = emdkManager;

		// Acquire the barcode manager resources
		barcodeManager = (BarcodeManager) emdkManager
				.getInstance(FEATURE_TYPE.BARCODE);

		// Add connection listener
		if (barcodeManager != null) {
			barcodeManager.addConnectionListener(CT8000Service.currentActivity);
		}

	}

	public static void setupDevice() {

		try {

			if (scanner == null) {
				initScanner();
			}

			if (scanner != null) {

				ScannerConfig config = scanner.getConfig();

				config.decoderParams.gs1Databar.enabled = true;
				config.decoderParams.code11.enabled = true;
				config.decoderParams.code39.enabled = true;
				config.decoderParams.code93.enabled = true;
				config.decoderParams.code128.enabled = true;
				config.decoderParams.dataMatrix.enabled = true;
				config.decoderParams.upca.enabled = true;
				config.decoderParams.upce0.enabled = true;
				config.decoderParams.upce1.enabled = true;
				config.decoderParams.ean8.enabled = true;
				config.decoderParams.ean13.enabled = true;

				config.decoderParams.aztec.enabled = true;
				config.decoderParams.codaBar.enabled = true;
				config.decoderParams.compositeAB.enabled = true;
				config.decoderParams.compositeC.enabled = true;
				config.decoderParams.matrix2of5.enabled = true;
				config.decoderParams.microPDF.enabled = true;
				config.decoderParams.msi.enabled = true;
				config.decoderParams.qrCode.enabled = true;
				config.decoderParams.pdf417.enabled = true;

				scanner.setConfig(config);
			}

		} catch (ScannerException e) {

			// StringWriter something = new StringWriter();
			// PrintWriter pw = new PrintWriter(something);
			// e.printStackTrace(pw);
			// something.toString();
			// Log.wtf("ScannerException - " + "setupDevice",
			// something.toString());
		} catch (Exception e) {

			StringWriter something = new StringWriter();
			PrintWriter pw = new PrintWriter(something);
			e.printStackTrace(pw);
			something.toString();
			Log.wtf("Exception - " + "setupDevice", something.toString());
		}
	}
}
