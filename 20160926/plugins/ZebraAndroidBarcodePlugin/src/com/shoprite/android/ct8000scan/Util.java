package com.shoprite.android.ct8000scan;

import android.app.ActivityManager;
import android.app.ActivityManager.RunningServiceInfo;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;

import com.virtualmobiletech.plugin.PluginActivity;
import com.virtualmobiletech.plugin.PluginActivityModel;
import com.virtualmobiletech.plugins.PluginUtils;

public class Util extends PluginActivityModel{
	
	@Override
	protected void doCreate(Bundle savedInstanceState) {
		super.doCreate(savedInstanceState);

		Log.wtf("Util", "doCreate");
		
		Class<CT8000Service> set = CT8000Service.class;
		
		if(!isMyServiceRunning(CT8000Service.class)){
			Log.wtf("isMyServiceRunning", "Was Not Running");
			PluginUtils.getInstance().getContext().startService(new Intent(PluginUtils.getInstance().getContext(), set));	
		}

		PluginActivity.getCurrentRampActivity().finishActivity(PluginUtils.IGNORE_REQUEST_CODE);
	}

	public boolean isMyServiceRunning(Class<?> serviceClass) {

		ActivityManager manager = (ActivityManager) PluginUtils.getInstance().getActivity().getSystemService(Context.ACTIVITY_SERVICE);
		for (RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {

//			Log.wtf("serviceClass", service.service.getClassName());
			
			if (serviceClass.getName().equals(service.service.getClassName())) {

				return true;
			}
		}
		return false;
	}

}
