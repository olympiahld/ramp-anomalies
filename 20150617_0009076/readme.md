# The Bug

There is an issue on Ramp iOS VM 5.0.0, where uxml elements dont get
their _expands_ right, they are either too big (take up too much
space), or collapsed.

# The App

This app has a simple form with ten input areas, however it is not
possible to view or scroll the entire form inside an app running on
Ramp iOS VM 5.0.0. The Ramp developer should remove some of the text
boxes too see how the behavior of the Ramp iOS vm 5.0.0 changes once
there is enough screen space to fit it all in.

* This is a screenshot from an android device that renders correctly:
  [android screenshot](android_screenshot.png)
* This is a screenshot from an ios device that gets it wrong:
  [iOS screenshot](ios_screenshot.jpg)

# Reported

By Johan on Friday, 5 June, 2015.
