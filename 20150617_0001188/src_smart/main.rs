// this example will indicate that the callback is still called +-12 times instead of "timerStopTask(@newTimer)" being executed.  
// the first callback("testTimer") should cancel the timer.


function main() {
  @timerRuns = 0;
  example.p1.text = "Ramp Version: " ++ rampGetProperty("config._ramp_version");
  example.p2.text = "OS Name: " ++ rampGetProperty("config._os_name");
  example.p3.text = "OS Version: " ++ rampGetProperty("config._os_version");
  example.p4.text = "Device Model: " ++ rampGetProperty("config._device_model");
  example.p5.text = "Device Make: " ++ rampGetProperty("config._device_make");
  @newTimer = timerStartTask("testTimer",500);
  @true = "false";
  @pt = strSub(timerGetDeviceTime(),4,13); // RAMP cannot use this as it's a 32 bit..  13 digit int ... I chop it
  @pt_plus_5s = @pt + 6000;
  counter = 0;
  while(@true != "true"){ // cannot use (counter != 10) - RAMP gives int error - no line number or var name in error
                          //            (counter < 10) works
     if(strSub(timerGetDeviceTime(),4,13) > @pt_plus_5s){
        @true = "true";  
     }  
     counter = counter +1;
  }
  goto("example");
}

function testTimer(){
  if(@newTimer!=""){
    timerStopTask(@newTimer);
    @newTimer = "";
  }
  // this is another work around to bypass all the callbacks - which still wastes resources
  // this example will indicate that the callback is still called +-12 times instead of "timerStopTask(@newTimer)" being executed.  
  // the first callback("testTimer") should cancel the timer.
  /**else{
    @newTimer = "";
    return;
  }
  * 
  */
  @timerRuns = @timerRuns +1;
  example.testTimer.text = "timer called : " ++ @timerRuns ++ " times!";   
  goto("example");
}

/* debugger:on */
//function debug(value) {webService("debugHandler", "device-time", timerGetDeviceTime(), "debug-value", value);}

