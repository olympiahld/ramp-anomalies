package com.shoprite.android.ct8000scan;

import java.util.Vector;

import android.app.Activity;
import android.content.Intent;

import com.virtualmobiletech.nativefunctions.APluginFunction;
import com.virtualmobiletech.plugin.PluginActivity;
import com.virtualmobiletech.plugins.PluginUtils;

public class CT8000ScanPlugin extends APluginFunction {
	static final String COMPANY_ID = "SQIM2";
	public static final String SET_SCANNER_CALLBACK = COMPANY_ID
			+ "_setScannerCallbackCT8000";
	public static final String ACTIVATE_SCANNER = COMPANY_ID
			+ "_activateScannerCT8000";
	public static final String DEACTIVATE_SCANNER = COMPANY_ID
			+ "_deactivateScannerCT8000";
	public static final String START_SERVICE = COMPANY_ID
			+ "_startServiceCT8000";

	public static Boolean scannerState = null;

	// RAMP does not support boolean type
	public static final String SUCCESS = "1";
	public static final String FAIL = "0";

	public CT8000ScanPlugin() {
		super(new String[] { SET_SCANNER_CALLBACK, ACTIVATE_SCANNER,
				DEACTIVATE_SCANNER, START_SERVICE });
	}

	public void setScannerState(Boolean scannerState) {
		CT8000ScanPlugin.scannerState = scannerState;
	}

	public Boolean getScannerState() {
		return scannerState;
	}

	@SuppressWarnings("rawtypes")
	@Override
	public Object invoke(String functionName, Vector args) {
		if (functionName.equals(ACTIVATE_SCANNER)) {
			PluginUtils.getInstance().runOnUiThread(new Runnable() {
				@Override
				public void run() {
					activateScanner();
				}
			});
			return SUCCESS;
		}

		if (functionName.equals(DEACTIVATE_SCANNER)) {
			PluginUtils.getInstance().runOnUiThread(new Runnable() {
				@Override
				public void run() {
					deactivateScanner();
				}
			});
			return SUCCESS;
		}

		if (functionName.equals(START_SERVICE)) {
			PluginUtils.getInstance().runOnUiThread(new Runnable() {
				@Override
				public void run() {
					startService();
				}
			});
			return SUCCESS;
		}

		if (functionName.equals(SET_SCANNER_CALLBACK)) {
			CT8000Activity.setCallback((String) args.elementAt(0));
		}

		return FAIL;
	}

	// Set the physical scanner on
	private void activateScanner() {

		setScannerState(true);

		Activity activity = PluginUtils.getInstance().getActivity();

		Intent intent = new Intent(activity, PluginActivity.class);
		intent = PluginActivity.assignPluginActivityModelToIntent(intent,
				CT8000Activity.class);
		activity.startActivityForResult(intent, PluginUtils.IGNORE_REQUEST_CODE);
	}

	// Set the physical scanner off
	private void deactivateScanner() {

		setScannerState(false);

		Activity activity = PluginUtils.getInstance().getActivity();

		Intent intent = new Intent(activity, PluginActivity.class);
		intent = PluginActivity.assignPluginActivityModelToIntent(intent,
				CT8000Activity.class);
		activity.startActivityForResult(intent, PluginUtils.IGNORE_REQUEST_CODE);
	}

	// Start Service
	private void startService() {

		Activity activity = PluginUtils.getInstance().getActivity();

		Intent intent = new Intent(activity, PluginActivity.class);
		intent = PluginActivity.assignPluginActivityModelToIntent(intent,
				Util.class);
		activity.startActivityForResult(intent, PluginUtils.IGNORE_REQUEST_CODE);
	}

	@Override
	public boolean isSupported() {

		try {
			Class.forName("com.shoprite.android.ct8000scan.CT8000Service");

			return true;
		} catch (Exception e) {
			return false;
		}
	}
}
