function main() {
  
  pluginFound = funcSupported("SQIM2_startServiceCT8000");
  
  if(pluginFound == "1") {
    SQIM2_setScannerCallbackCT8000("scanner_callback");
  }
  
  example.p1.text = "Ramp Version: " ++ rampGetProperty("config._ramp_version");
  example.p2.text = "OS Name: " ++ rampGetProperty("config._os_name");
  example.p3.text = "OS Version: " ++ rampGetProperty("config._os_version");
  example.p4.text = "Device Model: " ++ rampGetProperty("config._device_model");
  example.p5.text = "Device Make: " ++ rampGetProperty("config._device_make");
  example.p6.text = "Plugin Status: " ++ pluginFound;
  example.p7.text = "Scanner Callback: N/A";
  
  goto("example");
}

function start_service() {
  if(funcSupported("SQIM2_startServiceCT8000") == 1)
    SQIM2_startServiceCT8000();
}

function activate_scanner() {
  if(funcSupported("SQIM2_activateScannerCT8000") == 1)
    SQIM2_activateScannerCT8000();
}

function deactivate_scanner() {
  if(funcSupported("SQIM2_deactivateScannerCT8000") == 1)
    SQIM2_deactivateScannerCT8000();
}

function scanner_callback(a) {
  example.p7.text = "Scanner Callback: " ++ a;
}